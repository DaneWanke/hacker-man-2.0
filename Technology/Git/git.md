**Starting a new project**
1. Clone existing repo: git clone *http link*
2. git init, git commit -m 'Initial project version'
    - git remote add <.shortname> <.url> to connect to remote repo

**Clearing undesired code out**
- git reset --hard


---
**Working remotely**
- git fetch <.remote> will get new data but it will not update repo
- git pull <.remote> will get new data and merge with current code
- git push <.remote> <.branch> will push data to specified repo posi
- git remote show <.remote> will show you all repo details

**Branching is for testing and having operational code**