Always create a virtual env **python -m venv /path/**
    - To get into env, **cd venv folder** then run **/Scripts/activate/**
    - To get out run **deactivate**

<hr>
<hr>

- e = sys.exc_info()[0] <--- How to catch general case errors, does not give as accurate as information
- Python has a 1000 recursion max and will crash upon hitting it