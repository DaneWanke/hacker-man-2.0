alt txt always use even if just empty incase img cant load
Ask yourself if you removed all surrounding context, would that content still make sense? -> if not then use article
section is for grouping thematically related content

<hr>

- <.div> groups content
- <.section> groups related content 
- <.article> groups independent, self-contained content

<hr>

<.audio controls> <- controls brings up play, pause, scroller etc...
- <.source src='https://s3.amazonaws.com/freecodecamp/screen-reader.mp3' type='audio/mpeg'>
    <./audio> <- this is what audio looks like but should never auto play

<.fieldset> HTML element is used to group several controls as well as labels
- ^^Uses <.legend> for grouping

If using date input, this will not be supported in older browsers so a string to date mech should be implemented

<.time datetime=''> ... <./time>
<hr>

To show content in a screenreader but not show on page...
1. position: absolute
2. left: -10000px;;

setting visibility or display or size to 0 will not make it detectable for a screen reader

accesskey attribute to specify a shortcut key to activate or bring focus to an element

tabindex <- indicates an element can be focused on

negative tabindex value (typically -1) indicates that an element is focusable, but is not reachable by the keyboard
Elements are focused in order from tabindex value 1 first then onwards...

 Focusing an element will also give us access to :focus in css