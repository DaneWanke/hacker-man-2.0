- src points to something that needs to be added to a page while href points to a URL where you need a resource from
- Using href internally: use a hash symbol # plus the value of the id attribute for the element ex href="#html-atribute-id"
- target="_blank" makes the link load in a new tab
- Replace the href attribute value with a #, also known as a hash symbol, to create a dead link
- You can make elements into links by nesting them within an a element.
- ul for unordered list, ol for ordered list (numbered) and li for listed item
- form action="/url-where-you-want-to-submit-form-data will submit to server from html but probably better to put in javascript
- form can bundel crap together i.e input and submit button
- <.input type="text" required> will make the field required and ubsubmittable without

**Labels**
- When using radio buttons all related options should be nested in a label together - space between starting and ending of label is visual title
- All related radio buttons should have the same name attribute to create a radio button group - this will deselect prev selected etc...
<.label for="indoor"> indoor
    <.input id="indoor" type='radio' name='indoor-outdoor'/>
<./label>
<.label for="outdoor"> outdoor
    <.input id="outdoor" type='radio' name='indoor-outdoor'/>
<./label>

- setting value='vallue' will add it to the form
- checked attribute will make the box checked by default