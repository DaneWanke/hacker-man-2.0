display:flex; <-- to create flexbox
<hr>
flex-direction: row, column, reverse-row, reverse-column

Justify content:
- flex start: default, everything comes on after another on LHS
- center: centers elements
- flex end: everything comes right after another on RHS
- space-between: items are evenly spaced (items are on the ends)
- space-around: similar to space between but items are not locked at ends
- space-evenly: distributes elements evenly 

align items is like justify content only its for items in a flexbox:
- flex-start
- flex-end
- center
- stretch: takes up full container
- baseline: takes up just what is required for the text infront of it

flex-shrink <-shrinks stuff in a flexbox compared to other items in the flexbox

shorthand way to do flexgrow,shrink the basis (in order) is 'flex: 1 0 10px;'

align-self is like align items but at an indiviual level
