- font-family: FAMILY_NAME, GENERIC_NAME;
- When importing a font the family name is what you want to reference in font family and the generic name is for follow up font if 1set one is not availible

- boarder radius: #px;  - creates rounded corners can also use %, 50% makes it perfectly circular
- theres also style, color and width
- to style an id directly do #element-id { ... }

An element's padding controls the amount of space between the element's content and its border -> padding = space content takes up

An element's margin controls the amount of space between an element's border and surrounding elements -> margin = surronding white space relitive to other elements margins
Can use negative numbers as well here to go against other set elements properties or grow/shrink/move the element

Instead of specifying an element's padding-top, padding-right, padding-bottom, and padding-left properties individually, you can specify them all in one line clockwise (top, right, bottom, left) -> padding/margin: 5px 5px 5px 5px;

[attr=value] { ... } is another way to modify css 

The two main types of length units are absolute and relative.
bsolute units tie to physical units of length. For example, in and mm -> these will depend on screen size

Relative units, such as em or rem, are relative to another length value (There are several relative unit options that are tied to the size of the viewport)

Every html doc has a <.body> element
inherited, class, id, inline,!important (last style sheet declared for each of these are applied, not inline but in <.style>)

hex colors need to start with #RRGGBB (shorthand is #RGB)

  .orchid-text {
    color: rgb(218, 112, 214);
  } <- pretty color

  css vars --var: value;
  var(--var) to use this later on
  var(--var, otherVal) creates a fallback var (otherVal) incase the first one was not established or availible
  ^^ This can be useful for debugging


  background: red;
    background: var(--red-color);
    incase of browser compadability issues

    @media (max-width: 350px) {
    :root { <-- way to update global vars in a media querry
      /* Only change code below this line */
      --penguin-size: 200px;
      --penguin-skin: black;
      /* Only change code above this line */
    }


