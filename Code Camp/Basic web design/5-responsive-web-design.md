An example of a media query:
- @media (max-width: 100px) { /* CSS Rules */ }
<hr>

To make a responsive image:
- img {
  max-width: 100%;
  height: auto;
}

The max-width of 100% will make sure the image is never wider than the container it is in, and the height of auto will make the image keep its original aspect ratio.
<hr>

Making typing responsive:
- vw (viewport width): 10vw would be 10% of the viewport's width.
- vh (viewport height): 3vh would be 3% of the viewport's height.
- vmin (viewport minimum): 70vmin would be 70% of the - viewport's smaller dimension (height or width).
- vmax (viewport maximum): 100vmax would be 100% of the viewport's bigger dimension (height or width).