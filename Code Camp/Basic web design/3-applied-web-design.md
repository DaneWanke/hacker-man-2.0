<.strong>/<.b>, <.u>, <.s>, <.i>
<hr>
<.hr> creates lines
<hr>

The box-shadow property takes values for:
- offset-x (how far to push the shadow horizontally from the element),
- offset-y (how far to push the shadow vertically from the element),
- blur-radius,
- spread-radius 
- color

blur-radius and spread-radius values are optional

text-transform: lowercase, UPPERCASE, Capitilize, initial (default), inherhit(parent)

font-weight is how thick or bold a character is
line-height <- space between each line

a:action{} can change css properties during that action

block level items v.s inline items sit within surrounding content (like images or spans)

When the position of an element is set to relative, it allows you to specify how CSS should move it relative to its current position in the normal flow of the page. It pairs with the CSS offset properties of left or right, and top or bottom. These say how many pixels, percentages, or ems to move the item away from where it is normally positioned.

position: absolute; removes the element from the UI flow
position: fixed; like absolute but does not move when scrolling

float left and right pushes elements in a box togetehr (kind of like a shitier flexbox)

z-index: layer system in css

to center an element on screen set its margin as follows: margin: auto;

complementary colors cancel each others hex codes (or once added together will return FFFFFF)

tertiary colours is when you pick one dominate color and 2 different colors that are beside its complementary color 

linear-gradient(gradient_direction, color 1, color 2, color 3, ...)

looks pretty background: linear-gradient(35deg, #CCFFFF, #FFCCCC);

background: url(https://cdn-media-1.freecodecamp.org/imgr/MJAkxbh.png) <--Allows you to find background images at a url

scale() <-function that allows you to modify the size of an element <-cool effect for a hover?!
<hr>
Animation
#id { animation-name:..., animation-length:...}
@keyframes $animation-name {0%{...} 5%{...}...}
Can use keyframes to animate hovers too 
Animations have a wierd reset time <- to fix this go to the css identifier (not keyframes) and set the animation-fill-mode to forwards

to loop an animation: animation-iteration-count: 3; (can also supply 'infinite')
animation-timing-function can speed up an animation (linear or non-linear)