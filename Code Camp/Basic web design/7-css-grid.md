to create a grid: ----> display:grid;

grid-template-column: size of each column;

Defining grid sizes:
- You can use absolute and relative units like px and em in CSS Grid
- fr -> sets column or row to a fraction of availible space
- auto -> sets column/row to size of content automatically
- % -> sets column/row to % of parent container


grid-column-gap <-- puts gaps inbetween columns/rows
grid-gap (shorthand for the one above) - does row first then column

grid-column: 1 / 3;
This will make the item start at the first vertical line of the grid on the left and span to the 3rd line of the grid, consuming two columns.

align self also works with grid

grid-template-areas:
'a a a'
'a b b'
'c c c' This will set and predefine your grid
grid-area:name; will not set name to fill that area of the grid
or of course you can use the weird fraction notation (start and up to but not including) i.e <b>grid-area: horizontal line to start at / vertical line to start at / horizontal line to end at / vertical line to end at;</b>

repeat(100, 50px) creates 100 50px columns